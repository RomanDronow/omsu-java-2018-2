package simple_server;

import com.google.gson.Gson;
import person.Person;

import java.util.List;

class Server {
    private Gson gson = new Gson();
    private List<Person> persons;

    Server(List<Person> persons) {
        this.persons = persons;
    }

    String get(String request) {
        Person fromRequest = gson.fromJson(request, Person.class);
        for (Person p : persons) {
            if (fromRequest.getFirstName() != null) {
                if (!fromRequest.getFirstName().equals(p.getFirstName())) {
                    continue;
                }
            }
            if (fromRequest.getLastName() != null) {
                if (!fromRequest.getLastName().equals(p.getLastName())) {
                    continue;
                }
            }
            if (fromRequest.getAge() > 0) {
                if (fromRequest.getAge() != p.getAge()) {
                    continue;
                }
            }
            return gson.toJson(p);
        }
        return "{}";
    }

    void add(String request) {
        Person fromRequest = gson.fromJson(request, Person.class);
        if (fromRequest.getFirstName() != null && fromRequest.getLastName() != null && fromRequest.getAge() > 0)
            persons.add(fromRequest);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Person p : persons) {
            sb.append(p.toString());
            sb.append('\n');
        }
        return sb.toString();
    }
}
