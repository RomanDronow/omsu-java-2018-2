package simple_server;

public class Client {
    private Server current;

    Client(Server current) {
        this.current = current;
    }

    public void setCurrent(Server current) {
        this.current = current;
    }

    void add(String request) {
        current.add(request);
    }

    String get(String request) {
        return current.get(request);
    }
}
