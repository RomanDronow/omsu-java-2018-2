package socket_server;

import com.google.gson.Gson;
import person.Person;
import person.PersonJson;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class NewServer {
    private static Gson gson = new Gson();
    private static List<Person> personList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        int port = 25565;
        ServerSocket serverSocket = new ServerSocket(port);
        while (true) {
            System.out.println("server socket ready");
            Socket clientSocket = serverSocket.accept();
            System.out.println("client socket ready");
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            System.out.println("out ready");
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            System.out.println("in ready");
            boolean isConnected = true;
            while (isConnected) {
                String request = null;
                try {
                    request = in.readLine();
                } catch (SocketException e) {
                    isConnected = false;
                }
                if (isConnected) {
                    System.out.print("Request: " + request + "\n");
                    if (request.equalsIgnoreCase("quit")) {
                        System.out.println("Quit by client");
                        return;
                    }
                    if (request.equals("dc")) {
                        System.out.println("Client disconnected");
                        isConnected = false;
                        clientSocket.close();
                        out.close();
                        in.close();
                        break;
                    }
                    if (request.startsWith("add")) {
                        if (add(request.substring(3))) {
                            System.out.print("Add " + request.substring(3) + " to list\n");
                        } else {
                            System.out.print("Can't add. Bad request.");
                        }
                    }
                    if (request.startsWith("get")) {
                        try {
                            System.out.print("Sending object back by request: ");
                            String response = get(request.substring(3));
                            if (response.length() > 1) {
                                out.write(response + "\n");
                                out.flush();
                            }
                            System.out.print(request.substring(3));
                        } catch (IOException e) {
                            System.out.print("ERROR" + "\n");
                            return;
                        }
                    } else System.out.println("\nUnrecognized input\n");
                }
            }
        }
    }

    private static boolean add(String request) {
        Person p = gson.fromJson(request, Person.class);
        if (p.getFirstName() != null && p.getLastName() != null && p.getAge() > 0) {
            personList.add(p);
            return true;
        }
        return false;
    }

    private static String get(String request) {
        Person fromRequest = gson.fromJson(request, Person.class);
        for (Person p : personList) {
            if (fromRequest.getFirstName() != null) {
                if (!fromRequest.getFirstName().equals(p.getFirstName())) {
                    continue;
                }
            }
            if (fromRequest.getLastName() != null) {
                if (!fromRequest.getLastName().equals(p.getLastName())) {
                    continue;
                }
            }
            if (fromRequest.getAge() > 0) {
                if (fromRequest.getAge() != p.getAge()) {
                    continue;
                }
            }
            return PersonJson.personToJson(p);
        }
        return "{}";
    }
}
