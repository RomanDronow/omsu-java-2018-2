package socket_server;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        int port = 25565;
        Socket clientSocket = null;
        try {
            System.out.print("Client socket on localhost: ");
            clientSocket = new Socket(InetAddress.getLocalHost(), port);
            System.out.print("READY" + "\n");
        } catch (IOException e) {
            System.out.print("ERROR" + "\n");
            return;
        }
        BufferedWriter out = null;
        try {
            System.out.print("Output stream: ");
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            System.out.print("READY" + "\n");
        } catch (IOException e) {
            System.out.print("ERROR" + "\n");
            return;
        }
        BufferedReader in = null;
        try {
            System.out.print("Input stream: ");
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            System.out.print("READY" + "\n");
        } catch (IOException e) {
            System.out.print("ERROR" + "\n");
            return;
        }
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Reading string from console: ");
            String request = sc.nextLine();
            if ("quit".equalsIgnoreCase(request)) {
                System.out.println("BYE");
                break;
            }
            try {
                System.out.print("Sending: " + request + "\n");
                out.write(request + "\n");
            } catch (IOException e) {
                return;
            }
            try {
                System.out.print("Flushing: ");
                out.flush();
                System.out.print("DONE" + "\n");
            } catch (IOException e) {
                System.out.print("ERROR" + "\n");
                return;
            }
            if (request.startsWith("get")) {
                String response = null;
                try {
                    System.out.print("Got response: ");
                    response = in.readLine();
                    System.out.print(response + "\n");
                } catch (IOException e) {
                    System.out.print("ERROR" + "\n");
                    return;
                }
            }
        }
        out.write("dc\n");
    }
}