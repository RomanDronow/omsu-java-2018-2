package socket_server;

import com.google.gson.Gson;
import person.Person;
import person.PersonJson;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static person.PersonJson.JsonToPerson;

public class Server {
    private static Gson gson = new Gson();
    private static List<Person> personList = new ArrayList<>();

    static void work() {
        int port = 25565;
        ServerSocket serverSocket = null;
        try {
            System.out.print("Server socket: ");
            serverSocket = new ServerSocket(port);
            System.out.print("READY on port " + port + "\n");
        } catch (IOException e) {
            System.out.print("ERROR");
        }
        Socket clientSocket = null;
        try {
            System.out.print("Client socket: ");
            clientSocket = serverSocket.accept();
            System.out.print("READY" + "\n");
        } catch (IOException e) {
            System.out.print("ERROR" + "\n");
        }
        BufferedWriter out = null;
        try {
            System.out.print("Output stream: ");
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            System.out.print("READY" + "\n");
        } catch (IOException e) {
            System.out.print("ERROR" + "\n");
            return;
        }
        BufferedReader in = null;
        try {
            System.out.print("Input stream: ");
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            System.out.print("READY" + "\n");
        } catch (IOException e) {
            System.out.print("ERROR" + "\n");
            return;
        }
        while (true) {
            String request = null;
            while (request == null) {
                try {
                    System.out.print("Reading string: ");
                    request = in.readLine();
                    System.out.print(request + "\n");
                } catch (IOException e) {
                    System.out.print("ERROR" + "\n");
                    e.printStackTrace();
                    return;
                }
            }
            if (request.equalsIgnoreCase("quit")) {
                System.out.print("Quit by client");
                return;
            }
            if(request.equals("dc")){
                break;
            }
            if (request.startsWith("add")) {
                if (add(request.substring(3))) {
                    System.out.print("Add " + request.substring(3) + " to list\n");
                } else {
                    System.out.print("Can't add. Bad request.");
                }
            }
            if (request.startsWith("get")) {
                try {
                    System.out.print("Sending object back by request: ");
                    String response = get(request.substring(3));
                    if (response.length() > 1) {
                        out.write(response + "\n");
                        out.flush();
                    }
                    System.out.print(request.substring(3));
                } catch (IOException e) {
                    System.out.print("ERROR" + "\n");
                    return;
                }
            } else System.out.println("\nUnrecognized input\n");
        }
    }

    private static boolean add(String request) {
        Person p = gson.fromJson(request, Person.class);
        if (p.getFirstName() != null && p.getLastName() != null && p.getAge() > 0) {
            personList.add(p);
            return true;
        }
        return false;
    }

    private static String get(String request) {
        Person fromRequest = gson.fromJson(request, Person.class);
        for (Person p : personList) {
            if (fromRequest.getFirstName() != null) {
                if (!fromRequest.getFirstName().equals(p.getFirstName())) {
                    continue;
                }
            }
            if (fromRequest.getLastName() != null) {
                if (!fromRequest.getLastName().equals(p.getLastName())) {
                    continue;
                }
            }
            if (fromRequest.getAge() > 0) {
                if (fromRequest.getAge() != p.getAge()) {
                    continue;
                }
            }
            return PersonJson.personToJson(p);
        }
        return "{}";
    }

    public static void main(String[] args) {
        work();
    }
}
