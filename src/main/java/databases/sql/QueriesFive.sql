USE bookstore;

DELETE
FROM books
WHERE pages > 250; # удалить записи, где количество страниц которых больше P

DELETE
FROM books
WHERE title LIKE 'Harry%'; # название которых начинается со слова W

DELETE
FROM books
WHERE id BETWEEN 1 AND 3; # id которых от id1 до id2

DELETE
FROM books
WHERE title = 'War and Peace'; # с названием N

DELETE
FROM books
WHERE title = 'The Metamorphosis'
  AND title = 'War and Peace'
  AND title = 'The Black Obelisk'; # с названиями N1, N2 и N3


DELETE
FROM books; # все книги