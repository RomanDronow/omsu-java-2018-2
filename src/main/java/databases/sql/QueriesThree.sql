USE bookstore;

SELECT *
FROM books; # выбор всех

SELECT *
FROM books
ORDER BY id
LIMIT 8; # выбор первых N записей, N = 8

SELECT *
FROM books
WHERE id = 3; # выбор по id

SELECT *
FROM books
WHERE title = 'War and Peace'; # выбор по названию

SELECT *
FROM books
WHERE title LIKE 'W%'; # всех записей с названием, начинающимся со слова W

SELECT *
FROM books
WHERE title LIKE '%W'; # всех записей с названием, заканчивающимся словом W

SELECT *
FROM books
WHERE year > 1900
  AND year < 1980; # записей о книгах, изданных с года Y1 до года Y2

SELECT *
FROM books
WHERE pages BETWEEN 100 AND 600; # записей о книгах с количеством страниц от P1 до P2

SELECT *
FROM books
WHERE year BETWEEN 1900 AND 1980
  AND pages BETWEEN 100 AND 600; # записей о книгах с количеством страниц от P1 до P2, изданных с года Y1 до года Y2
