USE bookstore;

ALTER TABLE books
    ADD COLUMN publication_house VARCHAR(50) NULL AFTER year;

ALTER TABLE books
    ADD COLUMN binding ENUM ('without binding','hard binding','soft binding') AFTER publication_house;

# Изменить таблицу ‘books’, добавив следующие поля
# publishing_house // издательство
# binding // тип переплета
# Для поля binding предусмотреть варианты “без переплета”, “мягкий”, “твердый”. Возможно, со временем будут добавлены и иные типы.


UPDATE books
SET books.binding = 'without binding'; # Установить всем книгам в таблице тип переплета “без переплета”

UPDATE books
SET books.publication_house = 'Moskva Pechatniy Dom'
WHERE id between 2 and 3; # Установить книгам с id от ID1 до ID2 поле “издательство” в P

ALTER TABLE books
    CHANGE COLUMN title bookname VARCHAR(50); # Изменить название столбца ‘title’  на ‘bookname’

RENAME TABLE bookstore.books to bookstore.problems; # Изменить название таблицы.

DROP TABLE IF EXISTS problems; # Удалить таблицу