USE bookstore;

CREATE INDEX year ON books (year);
CREATE INDEX title ON books (title);
CREATE INDEX pages ON books (pages);
