DROP DATABASE IF EXISTS bookstore;
CREATE DATABASE bookstore;
USE bookstore;

CREATE TABLE books
(
    id INT(11) NOT NULL AUTO_INCREMENT,
    title VARCHAR(50) NOT NULL,
    year SMALLINT UNSIGNED NOT NULL,
    pages INT(11) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO books
VALUES (NULL, 'War and Peace', 1869, 1276);

INSERT INTO books
VALUES (NULL, 'Harry Potter and Philosopher''s Stone', 1997, 223);

INSERT INTO books
VALUES (NULL, 'The Metamorphosis', 1915, 102);

INSERT INTO books
VALUES (NULL, 'The Black Obelisk', 1956, 448);