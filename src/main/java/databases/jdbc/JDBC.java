package databases.jdbc;

import java.sql.*;
import java.util.Scanner;

public class JDBC {
    private static final String URL = "jdbc:mysql://localhost:3306/bookstore";
    private static final String USER = "user";
    private static final String PASSWORD = "password";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Unable to load JDBC driver.");
            e.printStackTrace();
            return false;
        }
    }

    private static void select(Connection c, String query) throws SQLException {
        Statement stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            System.out.println(rs.getInt("id") + " " + rs.getString("title") + " " +
                    rs.getInt("year") + " " + rs.getInt("pages"));
        }
    }

    private static void update(Connection c, String query) {
        try (Statement stmt = c.createStatement()) {
            stmt.execute(query);
            System.out.println("Query OK!");
        } catch (SQLException ignored) {
            System.out.println("Query not OK:(");
        }
    }

    public static void main(String[] args) throws SQLException {
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Scanner sc = new Scanner(System.in);
            while (true) {
                Statement statement = connection.createStatement();
                String query = sc.nextLine();
                if (query.equals("quit")) break;
                String[] tmp = query.split(" ");
                if (tmp[0].equalsIgnoreCase("select")) {
                    select(connection, query);
                } else update(connection, query);
            }
            sc.close();
        }
    }
}

