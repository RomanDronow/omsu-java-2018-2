package person;

import com.google.gson.Gson;

public class PersonJson {
    public static String personToJson(Person person) {
        Gson gson = new Gson();
        return gson.toJson(person, Person.class);
    }

    public static Person JsonToPerson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Person.class);
    }
}
