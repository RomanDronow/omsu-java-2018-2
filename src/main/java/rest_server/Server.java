package rest_server;


import com.google.gson.Gson;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import person.Person;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@SpringBootApplication
@RestController
public class Server {
    private static Gson gson = new Gson();
    private static List<Person> persons = new ArrayList<>();

    public static void main(String[] args) {
        persons.add(new Person("Artur", "Buss", 10));
        persons.add(new Person("Roman", "Grechka", 20));
        persons.add(new Person("Dima", "Tkachenko", 30));
        SpringApplication.run(Server.class, args);
    }

    @RequestMapping(method = GET)
    public Person get(@RequestParam(value = "firstName", required = false) String firstName,
                      @RequestParam(value = "lastName", required = false) String lastName,
                      @RequestParam(value = "age", required = false) int age) {
        Person fromRequest = new Person(firstName, lastName, age);
        for (Person p : persons) {
            if (!fromRequest.getFirstName().equals(null)) {
                if (!fromRequest.getFirstName().equals(p.getFirstName())) {
                    continue;
                }
            }
            if (!fromRequest.getLastName().equals("null")) {
                if (!fromRequest.getLastName().equals(p.getLastName())) {
                    continue;
                }
            }
            if (fromRequest.getAge() > 0) {
                if (fromRequest.getAge() != p.getAge()) {
                    continue;
                }
            }
            return p;
        }
        return null;
    }

    @RequestMapping(method = POST)
    public void post(@RequestBody String request){
        Person p = gson.fromJson(request, Person.class);
        if (p.getFirstName() != null && p.getLastName() != null && p.getAge() > 0) {
            persons.add(p);
        }
    }
}
