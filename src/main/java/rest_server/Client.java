package rest_server;

import com.google.gson.Gson;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import person.Person;

import java.util.HashMap;
import java.util.Map;

public class Client {
    private static String uri = "http://localhost:8080";
    private static RestTemplate restTemplate = new RestTemplate();
    private static Gson gson = new Gson();


    public static void main(String[] args) {
        postPerson("{\"firstName\":\"One\",\"lastName\":\"Two\",\"age\":3}");
        postPerson("{\"firstName\":\"Four\",\"lastName\":\"Five\",\"age\":6}");
        getPerson("One", "Two", 3);
        getPerson("Four", null, -1);
    }

    private static void postPerson(String json) {
        Person p = gson.fromJson(json, Person.class);
        restTemplate.postForObject(uri, p, Person.class);
    }

    private static void getPerson(String firstName, String lastName, int age) {
        String localUri = uri + "/?";
        if (firstName != null) {
            localUri += "firstName=" + firstName + "&";
        }
        if (lastName != null) {
            localUri += "lastName=" + lastName + "&";
        }
        if (age > 0) {
            localUri += "age=" + age;
        }
        ResponseEntity<Person> response = restTemplate.getForEntity(localUri, Person.class);
        System.out.println(response);
    }
}