package simple_server;

import org.junit.Test;
import person.Person;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TestClientServer {
    @Test
    public void testClientServer() {
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person("Ivan", "Ivanov", 15));
        System.out.println("Add Ivan Ivanov 15");
        persons.add(new Person("Ivan", "Ivanov", 12));
        System.out.println("Add Ivan Ivanov 12");
        persons.add(new Person("Oleg", "Petrov", 19));
        System.out.println("Add Oleg Petrov 19");
        persons.add(new Person("Kirill", "Kostuhin", 32));
        System.out.println("Add Kirill Kostuhin 32");
        persons.add(new Person("Peter", "Petrov", 22));
        System.out.println("Add Peter Petrov 22");
        Server s = new Server(persons);
        Client c = new Client(s);
        System.out.println();
        System.out.println("Current list:");
        System.out.println(s.toString());
        c.add("{\"firstName\":\"Roman\",\"lastName\":\"Dronov\",\"age\":20}");
        System.out.println("Add Roman Dronov 20 by JSON request");
        c.add("{\"firstName\":\"Artur\",\"lastName\":\"Buss\",\"age\":20}");
        System.out.println("Add Artur Buss 20 by JSON request");
        System.out.println();
        System.out.println("Current list:");
        System.out.println(s.toString());
        System.out.println("Get first with age 20:");
        System.out.println(c.get("{\"age\":20}"));
        System.out.println("Get first Petrov with age 22:");
        System.out.println(c.get("{\"lastName\":\"Petrov\",\"age\":22}"));
        System.out.println("Get first Ivan Ivanov 12:");
        System.out.println(c.get("{\"firstName\":\"Ivan\",\"lastName\":\"Ivanov\",\"age\":12}"));
        System.out.println("Get person not from list:");
        System.out.println(c.get("{\"firstName\":\"Nikto\",\"lastName\":\"Nekto\",\"age\":99}"));
    }
}
